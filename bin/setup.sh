#!/usr/bin/env bash

# Ownership
    echo "Setting up ownership..."
    sudo chown $USER.$USER /simple-cart -R

    # Setup server
    chmod +x install.sh
    ./install.sh
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    nvm install 8

    # Install packages
    echo "Setting up node_modules..."
    npm i
    npm i pm2 -g
    pm2 update
    npm i knex -g
    knex migrate:latest
    kenx seed:run

    # Start app
    echo "Setting up app..."
    pm2 start app.js --name="Backend"

    echo "Done"