require('dotenv').config()
import * as types from '../constants/ActionTypes';
import axios from 'axios'

export const viewCart = () => {
  return async dispatch => {
    dispatch({
      type: types.CART
    })
  }
}

export const actAddToCart = (product, quantity) => {
  return {
      type: Types.ADD_TO_CART,
      product,
      quantity
  }
}


export const actDeleteProductInCart = (product) => {
  return {
      type : Types.DELETE_PRODUCT_IN_CART,
      product
  }
}

export const actUpdateProductInCart = (product, quantity) => {
  return {
      type : Types.UPDATE_PRODUCT_IN_CART,
      product,
      quantity
  }
}