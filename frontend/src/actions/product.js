import * as types from '../constants/ActionTypes';
import axios from 'axios'


export const fetchProduct = () => {
  return async dispatch => {
    let response = await axios({
      method: 'GET',
      url: 'http://localhost:8080/products',
    })

    dispatch(success(response.data))
  }

  function success(products) {
    return {
      type: types.FETCH_PRODUCT,
      products
    }
  }
}
