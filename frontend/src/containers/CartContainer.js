import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cart from '../components/Cart'
import CartItem from '../components/CartItem';
import CartResult from '../components/CartResult';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { actDeleteProductInCart, actUpdateProductInCart } from '../actions/cart';

class CartContainer {
  render() {
    let { cart } = this.props
    return (
      <Cart>
        {this.showCartItem(cart)}
        {this.showTotalAmount(cart)}
      </Cart>
    )
  }

  showCartItem = (cart) => {
    var { onDeleteProductInCart, onUpdateProductInCart } = this.props;
    if (cart.length > 0) {
        result = cart.map((item, index) => {
            return (
                <CartItem
                    key={index}
                    item={item}
                    index={index}
                    onDeleteProductInCart={onDeleteProductInCart}
                    onUpdateProductInCart={onUpdateProductInCart}
                />
            );
        });
    }
    return result;
  }

  showTotalAmount = (cart) => {
      var result = null;
      if (cart.length > 0) {
          result = <CartResult cart={cart} />
      }
      return result;
  }
}

const mapStateToProps = state => {
  return {
    cart: state.cart
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onDeleteProductInCart: (product) => {
        dispatch(actDeleteProductInCart(product));
    },
  
    onUpdateProductInCart: (product, quantity) => {
        dispatch(actUpdateProductInCart(product, quantity));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer)
