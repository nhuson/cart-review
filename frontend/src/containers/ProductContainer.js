import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Product from '../components/Products'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { fetchProduct } from '../actions/product'
import ProductItem from '../components/ProductItem'

class ProductContainer extends Component {
  render() {
    let { products } = this.props
    console.log(products);
    return (
      <Product>{ this.renderProduct(products) }</Product>
    )
  }

  renderProduct(products) {
    let result = null;
    if (products.length > 0) {
      result = products.map((sumItem, index) => {
        return (
          <ProductItem
            key={index}
            product={sumItem}
          />
        );
      });
    }
    return result;
  }

  componentDidMount() {
    this.props.fetchProduct();
  }
}

const mapStateToProps = state => {
  return {
    products: state.product
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchProduct: () => {
      dispatch(fetchProduct())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer)
