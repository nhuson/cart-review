import React from 'react';
import { Route, Router } from 'react-router';
import ProductContainer from './containers/ProductContainer';
import CartContainer from './containers/CartContainer';

export default (
	<Router>
		<Route path="/" component={ProductContainer} />
		<Route path="/cart" component={CartContainer} />
	</Router>
);	