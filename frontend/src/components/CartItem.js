import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class CartItem extends Component {
    render() {
        var { item } = this.props;
        var { qty } = item;
        return (
            <tr>
              <td>John</td>
              <td>Doe</td>
              <td>john@example.com</td>
              <td>{ this.showSubTotal(item.price, item.qty) }</td>
              <td><button onClick={() => this.onUpdateQuantity(item.product, item.qty - 1)}>ー</button><input type="number"/><button onClick={() => this.onUpdateQuantity(item.product, item.qty + 1)}>＋</button></td>
              <td><button onClick={() => this.onDelete(item.product)}>ー</button></td>
            </tr>
        )
    }

    onUpdateQuantity = (product, quantity) => {
        if (quantity > 0) {
            var { onUpdateProductInCart, onChangeMessage } = this.props;
            onUpdateProductInCart(product, quantity);
            onChangeMessage(Message.MSG_UPDATE_CART_SUCCESS);
        }
    }

    onDelete = (product) => {
        var { onDeleteProductInCart, onChangeMessage } = this.props;
        onDeleteProductInCart(product);
        onChangeMessage(Message.MSG_DELETE_PRODUCT_IN_CART_SUCCESS);
    }

    showSubTotal = (price, quantity) => {
        return price * quantity;
    }
}

export default CartItem