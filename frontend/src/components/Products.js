import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/product.css';
import { Link } from 'react-router'

class Products extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="well well-sm">
            <strong>Category Title</strong>
            <strong><Link to="/cart">View Cart</Link></strong>
          </div>
          <div id="products" className="row list-group">
              { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}

export default Products;
