import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class ProductItem extends Component {
  render() {
    let { product } = this.props
    return (
      <div className="item grid-group-item col-xs-4 col-lg-4">
        <div className="thumbnail">
          <img className="group list-group-image" src="{ product.image }" alt="{ product.title }" />
          <div className="caption">
            <h4 className="group inner list-group-item-heading">{ product.title }</h4>
            <p className="group inner list-group-item-text">{ product.desc }</p>
            <div className="row">
              <div className="col-xs-12 col-md-6">
                <p className="lead">${ product.price }</p>
              </div>
              <div className="col-xs-12 col-md-6">
                <a className="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ProductItem
