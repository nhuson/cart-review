import * as types from '../constants/ActionTypes';
const initialState = [];

const product = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_PRODUCT:
      return Object.assign({}, state, {
        products: action.products
      })

    default:
    return state;
  }
}

export default product;