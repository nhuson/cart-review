import { combineReducers  } from 'redux';
import { routerReducer } from 'react-router-redux'
import product from './product'

const reducers = combineReducers({
	product,
	routing: routerReducer
});

export default reducers;
