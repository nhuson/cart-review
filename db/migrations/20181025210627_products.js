
exports.up = function(knex, Promise) {
  return knex.schema.createTable('products', function(t) {
    t.increments('id').unsigned().primary();
    t.string('title', 100).index();
    t.integer('price').index();
    t.text('desc');
    t.string('image');
    t.dateTime('created_at').default(null);
    t.dateTime('updated_at').default(null);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('products');
};
