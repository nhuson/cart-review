
exports.up = function(knex, Promise) {
  return knex.schema.createTable('product_order', function(t) {
    t.increments('id').unsigned().primary();
    t.integer('product_id').index();
    t.integer('order_id').index();
    t.integer('qty').index();
    t.dateTime('created_at').default(null);
    t.dateTime('updated_at').default(null);
  });
};

exports.down = function(knex, Promise) {
  
};
