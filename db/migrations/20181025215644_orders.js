
exports.up = function(knex, Promise) {
  return knex.schema.createTable('orders', function(t) {
    t.increments('id').unsigned().primary();
    t.text('total_price');
    t.dateTime('created_at').default(null);
    t.dateTime('updated_at').default(null);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('orders');
};
