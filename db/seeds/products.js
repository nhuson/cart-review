const faker = require("faker")

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(function () {
      // Inserts seed entries
      return knex('products').insert([
        {id: 1, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()},
        {id: 2, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()},
        {id: 3, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()},
        {id: 4, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()},
        {id: 5, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()},
        {id: 6, title: faker.commerce.productName(), price: faker.commerce.price(), desc: faker.random.words(), image: faker.random.image()}
      ]);
    });
};
