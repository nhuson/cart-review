require('dotenv').config()
const express = require("express")
const app = express()
const server = require('http').Server(app)
server.listen(process.env.APP_PORT)
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const knex = require('./db/database')

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.get('/products', async (req, res, next) => {
  try {
    let products = await knex.select().table('products')
    res.json(200, {
      status: true,
      data: products,
      message: "get product success"
    })
  } catch(err) {
    res.status(400).json({ error: 'Server error' })
  }
})

app.get('/orders', async (req, res, next) => {
  try {
    let orders = await knex.select().table('orders')
    res.json(200, {
      status: true,
      data: orders,
      message: "get product success"
    })
  } catch(err) {
    res.status(400).json({ error: 'Server error' })
  }
})

app.get('/orders/:order_id', async (req, res, next) => {
  try {
    let orders = await knex.select('products.title', 'products.price', 'products.image', 'orders.total_price', 'product_order.qty', 'orders.id')
    .from('orders')
    .innerJoin('product_order', function() {
      this.on('orders.id', '=', 'product_order.order_id')
    })
    .innerJoin('products', function() {
      this.on('products.id', '=', 'product_order.product_id')
    })
    .where('orders.id', req.params.order_id)

    res.json(200, {
      status: true,
      data: orders,
      message: "Get product success"
    })
  } catch(err) {
    console.log(err)
    res.status(400).json({ error: 'Server error' })
  }
})

app.post('/orders', async (req, res, next) => {
  try {
    let { total_price, products } = req.body
    let order = await knex.insert({ total_price: total_price }).into('orders')
    if (products && products.length > 0) {
      products.map(async product => {
        let productOrder = await knex.insert({ product_id: product.product_id, qty: product.qty, order_id: order[0] }).into('product_order')
      })
    }
    res.json(200, {
      status: true,
      message: "Order success"
    })
  } catch(err) {
    res.status(400).json({ error: 'Server error' })
  }
})